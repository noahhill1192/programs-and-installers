`readme.md`
`Noah Hill`
`git clone https://www.gitlab.com/noahhill1192/programs-and-installers`


# Programs and installers
## Noah Hill 
## What is in this repo
- This repo is maintained by Noah Hill
- I've made a lot of basic scripts and programs in my free time as an excuse to teach myself more and more. Unless I have made programs for another repo, either mine or somebody elses, all programs and scripts are located in this repo

# Feedback and support
## Should you submit feedback, bug reports, or commits?
- YES! I'll look through them and take feedback to make better code
## Will I edit and reupload the code?
- It depends, same for my other repos
- If there is a massive bug or I want to reupload it I will, but otherwise know even if I don't reupload it, I am still taking all the feedback I can get
## Can you commit code fixes myself?
- I will look through your code and probably accept it into the repo yes

# Licenses
## At the moment nothing on my GitLab repos has a license
- Everything is basic code anyone could make
    - Feel free to do what you want with it I don't care much
