#!/bin/bash

###############################
# Noah Hill
# Date: May 23, 2024
# Description: Simple privite program for running Fedora system updates 
# 
#     #                         #     #                 
##    #  ####    ##   #    #    #     # # #      #      
# #   # #    #  #  #  #    #    #     # # #      #      
#  #  # #    # #    # ######    ####### # #      #      
#   # # #    # ###### #    #    #     # # #      #      
#    ## #    # #    # #    #    #     # # #      #      
#     #  ####  #    # #    #    #     # # ###### ######


###############################


### ENV EXPORTS ###


# Check for package managers and set flags
check_package_managers() {
  declare -A package_managers=( ["dnf"]="dnf --version" ["flatpak"]="flatpak --version" ["snap"]="snap --version" ["nix"]="nix --version" ["cargo"]="cargo --version" )
  for manager in "${!package_managers[@]}"; do
    if command -v $manager &>/dev/null; then
      version=$(${package_managers[$manager]} | head -n 1 | awk '{print $NF}')
      export "${manager^^}_MANAGER"=true
      export "${manager^^}_VERSION"=$version
    fi
  done
}



## FLATPAK repos
check_flatpak_repos() {
  if [ -n "$FLATPAK_MANAGER" ]; then
    repo_counter=0
    echo "Flatpak repositories:"
    while IFS= read -r repo; do
      # Replace special characters with underscores
      sanitized_repo=$(echo "$repo" | tr -cs '[:alnum:]' '_')
      export "FLATPAK_REPO_$sanitized_repo"=true
      repo_counter=$((repo_counter + 1))
      echo -e "\033[42m$repo\033[0m"
    done < <(flatpak remotes)
    echo -e "\033[1;31mFound $repo_counter Flatpak repositories\033[0m."
  fi
}

## DNF repos
check_dnf_repos() {
  # Initialize repo_counter
  repo_counter=0

  echo "Enabled DNF repositories:"
  # Loop through the output of the dnf repolist enabled command
  while read -r repo_id repo_name; do
    # Skip the header or any lines that don't contain a repository ID
    if [[ $repo_id != "repo" && $repo_id != "id" ]]; then
      repo_counter=$((repo_counter + 1))
      # Create variable with DNF_REPO_ prefix and counter
      eval "DNF_REPO_${repo_counter}=${repo_id}"
      echo -e "\033[42m$repo_name\033[0m"
    fi
  done < <(sudo dnf repolist enabled | tail -n +2)

  # Inform user about the number of enabled repos
  echo -e "\033[1;31mFound $repo_counter enabled DNF repositories\033[0m."
}

## Snap repos
check_snap_repos() {
  # Initialize snap_counter
  local snap_counter=0

  # Loop through the output of the snap list command
  while read -r snap_name snap_version snap_rev snap_tracking snap_publisher snap_notes; do
    # Skip the header or any lines that don't contain a snap name
    if [[ $snap_name != "Name" ]]; then
      snap_counter=$((snap_counter + 1))
      # Create variable with SNAP_PACKAGE_ prefix and counter
      eval "SNAP_PACKAGE_${snap_counter}=${snap_name}"
    fi
  done < <(snap list | tail -n +1)

  # Inform user about the number of installed snaps
  echo -e "\033[1;31mFound $snap_counter installed Snap packages\033[0m."
}

### START OF PROGRAM ###
# Display welcome message and program info
welcome() {
	clear
	echo "Welcome to the Rudeamentory TUI program!"
	echo "This program allows you to perform various functions with a simple text-based interface."
	echo "Author: Noah Hill"
	echo "--------------------"
	echo "current date: $(date)"

	# Pause for user input
	read -p "Press enter to continue..."

	# Display available functions and their descriptions
	echo "Available functions:"
	echo "1. Install program - Installing a new program"
	echo "2. Update system - Updating a program"
	echo "3. Manage software repos"
	echo "4. Links"
  echo "5. System Info"
	echo "6. Exit program"

	# Prompt user for input
	read -p "Please enter the number of the function you would like to perform: " option
}

# Calling welcome
welcome

### MORE FUNCTIONS ###

#Manage software repos
manage_repo() {
	echo "Which package manager would you like to manage the repos for"
	echo "1. DNF"
	echo "2. Flatpak"
	echo "3. Snap"

	read -p "Please enter number 1-3" option

	# Perform action based on user input
	case $option in
    	1)
        	echo "Selected DNF"
        	sleep 2
        	read -p "Select 1 for repo list, select 2 for adding repositories" option

        	#Nested case statements
        	case $option in
        		1)
					check_dnf_repos

				;;

				2)
					echo "comeing soon"

				;;

				*)
					echo "Please Select 1 or 2"

				;;
			esac
			;;
    	2)
 			echo "Selected Flatpak"
        	sleep 2
        	read -p "Select 1 for repo list, select 2 for adding repositories" option

        	#Nested case statements
        	case $option in
        		1)
					check_flatpak_repos

				;;

				2)
					echo "comeing soon"

				;;

				*)
					echo "Please Select 1 or 2"

				;;
			esac
			;;

    	3)
			echo "Selected Snap"

        	;;
    	*)
        echo "Invalid input, please try again."
        manage_repo
        ;;
	esac
}


# General function to handle software installation
install_program() {
  check_package_managers
  read -p "Type the name of the program you want to install: " program_name
  declare -A package_managers=( ["dnf"]="sudo dnf install -y" ["flatpak"]="flatpak install" ["snap"]="sudo snap install" )
  for manager in "${!package_managers[@]}"; do
    if [ -n "${manager^^}_MANAGER" ]; then
      ${manager} search "$program_name" || echo "Program not found with $manager"
      read -p "Do you want to install $program_name with $manager? (y/n): " install_choice
      if [[ "$install_choice" == "y" ]]; then
        ${package_managers[$manager]} "$program_name"
      fi
    fi
  done
}


# General function to handle system updates
update_system() {
  check_package_managers
  declare -A update_commands=( ["dnf"]="sudo dnf update && sudo dnf upgrade" ["flatpak"]="flatpak update" ["snap"]="sudo snap update" )
  for manager in "${!update_commands[@]}"; do
    if [ -n "${manager^^}_MANAGER" ]; then
      echo "Updating $manager packages..."
      ${update_commands[$manager]}
    fi
  done
  echo "System update complete."
}

# Providing links for the user
links() {
  echo "Noah Hill"
  echo "---------"
  echo "GitLab:  https://gitlab.com/noahhill1192"
  echo "Twitch:  https://twitch.tv/kizsyr"
  echo "YouTube: https://www.youtube.com/channel/UCSNWPjIIwuiwTevsSpvM-TQ"
  echo -e "\nDepending on your terminal you may or may not be able to click on these links"
}

# System info
system_info() {
  # Checking to see if the user wants to install fastfetch
  read -p "Would you like to install fastfetch [y/N]" option
  case $option in
      y)
        # Installing fastfetch with DNF
        sudo dnf install -y fastfetch
        echo -e "This function will now run again\nMake sure to answer no"
        sleep 1
        system_info
      ;;

      n)
        echo "System Info"
        echo "-----------"
        echo
        echo "Date and Time: $(date)"
        echo
        echo "Processor Type: $(uname -p)"
        echo "Hardware Platform: $(uname -i)"
        echo "Operating System: $(uname -o)"
        echo "Kernel Name and Version: $(uname -s)"
      ;;

      *)
        echo "Please Select 1 or 2"

      ;;
  esac

}

# Perform action based on user input
case $option in
    1) install_program ;;
    2) update_system ;;
    3) manage_repo ;;
    4) links ;;
    5) system_info ;;
    6) exit 0 ;;
    *) echo "Invalid input, please try again." ;;
  esac