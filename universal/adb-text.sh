#!/bin/bash

usage() {
  echo "Usage: $0 TEXT..."
  echo "Sends TEXT to the connected ADB device."
  # echo "  --delay=SECONDS: Delay between keystrokes."
  echo "--delay=<SECONDS> | Adds delay between keystrokes | currently disabled (Won't do anything if used)"
  echo "--help            | Displays this help menu"
  echo "--doc             | Opens the ADB docs online for ADB"
  echo "                  | Note: --doc uses xdg-open. Ensure your default web browser is set in your desktop environment!"
  echo "                  | Note: --doc -m <browser> will open the docs in a custom browser"
  echo "                  | You may need to use --doc -m 'open -a <browser>' for MacOS"
  exit 0
}


# Check for `--help` flag
if [[ "$1" == "--help" ]]; then
  usage
  exit 0
fi

# Check for `--doc` or `--doc -m` flag
if [[ "$1" == "--doc" ]]; then
  shift
  if [[ "$1" == "-m" ]]; then
    shift
    if [[ -z "$1" ]]; then
      echo "Error: Please specify a browser after -m."
      exit 1
    fi
    browser="$1"
    shift
    "$browser" "https://developer.android.com/studio/command-line/adb"
  else
    read -rp "Open ADB documentation in your browser? (Y/n): " response
    if [[ "$response" =~ ^(yes|y|)$ ]]; then
      xdg-open "https://developer.android.com/studio/command-line/adb"
    fi
  fi
  exit 0
fi

# Check for ADB installation
if ! command -v adb &> /dev/null; then
  echo "Error: ADB is not installed."
  echo "Please install ADB according to your Linux distribution:"
  echo "- Ubuntu/Debian: sudo apt install adb"
  echo "- Fedora/CentOS: sudo dnf install android-tools"
  echo "- Arch Linux: sudo pacman -S android-tools"
  echo "- Other distributions: Refer to your package manager's documentation."
  exit 1
fi

# Check for ADB device
if ! adb devices | grep -q "device$"; then
  echo "Error: No ADB device connected."
  echo "Please refer to the official Android documentation for setting up ADB:"
  echo "https://developer.android.com/studio/command-line/adb"
  echo ""
  echo "running the '--doc' flag will open this link in your default browser"
  echo "Make sure your default browser is set in your DE"
  exit 1
fi

# Check for `--delay=<SECONDS>` flag
# delay=0
# if [[ "$1" =~ ^--delay=([[:digit:]]+)$ ]]; then
# delay="${BASH_REMATCH[1]}"
# shift
# fi

if [[ -z "$1" ]]; then
  usage
  exit 0
fi

# Send text with space conversion and optional delays
for text in "$@"; do
  text_with_percents="${text// /%s}"  # Replace spaces with "%s"
  for char in "$text_with_percents"; do
    if [[ "$char" == "%" ]]; then
      adb shell input keyevent KEYCODE_SPACE
      if [[ "$delay" -gt 0 ]]; then
        sleep "$delay"
      fi
    else
      adb shell input text "$char"
      echo "Text sent to device"
    fi
  done
done
